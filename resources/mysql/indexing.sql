create database gps_indexing ;
use gps_indexing ;
create table decision_index (id bigint not null, clef varchar(255), deleted datetime, document_key_suffix varchar(255), indexed datetime, outdated bit, version integer, root_key_id bigint, primary key (id)) ;
create table decision_index_tags (decisions_id bigint not null, tags_id bigint not null, primary key (decisions_id, tags_id)) ;
create table decision_modification (id bigint not null auto_increment, author_email varchar(255), author_name varchar(255), current_property_id bigint, current_value varchar(255), date datetime, index_id bigint, deletion bit, insertion bit, metadata_id bigint, replaced_property_id bigint, replaced_value varchar(255), primary key (id)) ;
create table decision_reservation (id bigint not null auto_increment, created datetime not null, indexing_id bigint not null, user_id smallint, primary key (id)) ;
create table decision_root_key (id bigint not null auto_increment, created TIMESTAMP DEFAULT CURRENT_TIMESTAMP not null, value varchar(255) not null, primary key (id)) ;
create table decision_root_key_record (id bigint not null auto_increment, date datetime, decision_id bigint not null, root_key_id bigint not null, primary key (id)) ;
create table decision_tag (id bigint not null auto_increment, created datetime, name varchar(255), organization_id bigint, primary key (id)) ;
create table denied_inet_address (id bigint not null auto_increment, active bit not null, allowed_by varchar(255), allowed_date date, denied_by varchar(255), denied_date date, address varchar(45) not null, end_address varchar(45), primary key (id)) ;
create table human_index (id bigint not null auto_increment, category_id bigint, keywords VARCHAR(4096), reserved VARCHAR(4096), created datetime not null, decision_id bigint, deleted datetime, document_key varchar(255) not null, modified datetime, created_by_id smallint, deleted_by_id smallint, modified_by_id smallint, root_key_id bigint, primary key (id)) ;
create table human_index_category (id MEDIUMINT not null auto_increment, display_order integer, extra varchar(255), label varchar(255) not null, level integer not null, parent_id MEDIUMINT, primary key (id)) ;
create table organization (id SMALLINT not null auto_increment, abbreviation varchar(16) not null, internal bit not null, name varchar(255) not null, primary key (id)) ;
create table organization_profiles (organization_id SMALLINT not null, profiles_id bigint not null) ;
create table profile (id bigint not null auto_increment, description varchar(255), internal bit not null, name varchar(255), primary key (id)) ;
create table profile_permissions (profile_id bigint not null, permissions varchar(255)) ;
create table user (id smallint not null auto_increment, enabled bit, full_name varchar(255) not null, password varchar(255) not null, username varchar(255), organization_id SMALLINT not null, primary key (id)) ;
create table user_activity (id bigint not null auto_increment, comment varchar(255), date datetime, ip varchar(45) not null, ip_city varchar(45), ip_country_code varchar(3), ip_country_name varchar(59), ip_latitude double precision, ip_longitude double precision, ip_region_code varchar(2), ip_region_name varchar(45), ip_zipcode varchar(11), username varchar(64), primary key (id)) ;
create table user_allowed_addresses (user_id smallint not null, address varchar(45) not null, end_address varchar(45)) ;
create table user_notification (id bigint not null auto_increment, created datetime not null, notified datetime not null, query varchar(255) not null, user_id bigint, primary key (id)) ;
create table user_profiles (user_id smallint not null, profiles_id bigint not null) ;
alter table decision_root_key add constraint UK_pvp8456keh1aubpaw5yq7f0kx unique (value) ;
alter table organization_profiles add constraint UK_9mj0tk2ells8g8gc5t48ioosl unique (profiles_id) ;
alter table user add constraint UK_t8tbwelrnviudxdaggwr1kd9b unique (username) ;
alter table decision_index add constraint FK_pgb9ljhlehga1qfdme7iweoo3 foreign key (root_key_id) references decision_root_key (id) ;
alter table decision_index_tags add constraint FK_k8hvljm1xinp94mumn20f03nu foreign key (tags_id) references decision_tag (id) ;
alter table decision_index_tags add constraint FK_37at6lqqom6tso5w51841hchk foreign key (decisions_id) references decision_index (id) ;
alter table decision_modification add constraint FK_eiabffeer7xd19ukjv5elu9uk foreign key (index_id) references decision_index (id) ;
alter table decision_reservation add constraint FK_d2e95lsjrc1ru8ogwsjqbcw8d foreign key (indexing_id) references decision_index (id) ;
alter table decision_reservation add constraint FK_k54imbag1ereaee8rq1hywipn foreign key (user_id) references user (id) ;
alter table decision_root_key_record add constraint FK_68igoa0pkpk435nqrcajsdcr0 foreign key (decision_id) references decision_index (id) ;
alter table decision_root_key_record add constraint FK_i0tv4cn4w1fwxxdpos0oaquo1 foreign key (root_key_id) references decision_root_key (id) ;
alter table decision_tag add constraint FK_jec3g1qjm1j7w3o65llb0tyfx foreign key (organization_id) references organization (id) ;
alter table human_index add constraint FK_kyejq74y1agigsa68bseeaq6s foreign key (category_id) references human_index_category (id) ;
alter table human_index add constraint FK_qak25m0ecitn7ot64ckvmju8e foreign key (created_by_id) references user (id) ;
alter table human_index add constraint FK_l1u5gpcx8kwjsp4foqmxxy44i foreign key (decision_id) references decision_index (id) ;
alter table human_index add constraint FK_ogq5j91qy01u5m089t79neuk2 foreign key (deleted_by_id) references user (id) ;
alter table human_index add constraint FK_b8l7vkbrn0ah7616vslpjyhbv foreign key (modified_by_id) references user (id) ;
alter table human_index add constraint FK_1muvg49f1ekd4fnnjyfubbfp8 foreign key (root_key_id) references decision_root_key (id) ;
alter table human_index_category add constraint FK_a18kiqpfkixbjs7sd84n3q8wy foreign key (parent_id) references human_index_category (id) ;
alter table organization_profiles add constraint FK_9mj0tk2ells8g8gc5t48ioosl foreign key (profiles_id) references profile (id) ;
alter table organization_profiles add constraint FK_7qoshw2yss335kqpn0epv7348 foreign key (organization_id) references organization (id) ;
alter table profile_permissions add constraint FK_kqh3un6l3qkknket48lr1gr9y foreign key (profile_id) references profile (id) ;
alter table user add constraint FK_n3q5mbs97yqmvsah7yyji1i78 foreign key (organization_id) references organization (id) ;
alter table user_allowed_addresses add constraint FK_8nhealfsw62u549bwnj34rm0f foreign key (user_id) references user (id) ;
alter table user_notification add constraint FK_hrv2lmyjlt3ken6hk2f4sg1e foreign key (user_id) references user (id) ;
alter table user_profiles add constraint FK_s90pqydyx72q26p3favr6qwpg foreign key (profiles_id) references profile (id) ;
alter table user_profiles add constraint FK_e5h89rk3ijvdmaiig4srogdc6 foreign key (user_id) references user (id) ;
INSERT INTO organization (id, abbreviation, internal, name) VALUES (1, 'EL',     true, 'Éditions Législatives') ;
INSERT INTO organization (id, abbreviation, internal, name) VALUES (2, 'EFL',    true, 'Éditions Francis Lefebvre') ;
INSERT INTO organization (id, abbreviation, internal, name) VALUES (3, 'DALLOZ', true, 'Éditions Dalloz') ;
INSERT INTO organization (id, abbreviation, internal, name) VALUES (4, 'ELS',    true, 'Éditions Lefebvre Sarrut') ;
INSERT INTO profile (id, description, internal, name) VALUES (1, 'Super Administrator', true, 'SUPERADMIN') ;
INSERT INTO profile (id, description, internal, name) VALUES (2, 'Administrator',       true, 'ADMIN') ;
INSERT INTO profile (id, description, internal, name) VALUES (3, 'Indexer',             true, 'INDEXER') ;
INSERT INTO profile (id, description, internal, name) VALUES (4, 'Viewer',              true, 'VIEWER') ;
INSERT INTO user (id, enabled, full_name, password, username, organization_id) VALUES (1, true, 'Super Administrator', 'AKg8jCakGOt+$R9hvvyca4YPS4Y0c+vyttg==', 'sa',      1) ;
INSERT INTO user (id, enabled, full_name, password, username, organization_id) VALUES (2, true, 'Administrator',       'AKg8jCakGOt+$R9hvvyca4YPS4Y0c+vyttg==', 'admin',   1) ;
INSERT INTO user (id, enabled, full_name, password, username, organization_id) VALUES (3, true, 'Indexer',             'AKg8jCakGOt+$R9hvvyca4YPS4Y0c+vyttg==', 'indexer', 2) ;
INSERT INTO user (id, enabled, full_name, password, username, organization_id) VALUES (4, true, 'Indexer',             'AKg8jCakGOt+$R9hvvyca4YPS4Y0c+vyttg==', 'els_auto_indexer',     4) ;
INSERT INTO user_profiles (user_id, profiles_id) VALUES (1, 1) ;
INSERT INTO user_profiles (user_id, profiles_id) VALUES (2, 2) ;
INSERT INTO user_profiles (user_id, profiles_id) VALUES (3, 3) ;
INSERT INTO user_profiles (user_id, profiles_id) VALUES (4, 3) ;
