create database gps_datastore ;
use gps_datastore ;
create table decision (id INT not null auto_increment, created TIMESTAMP DEFAULT CURRENT_TIMESTAMP not null, modified datetime, presence varchar(16) not null, version SMALLINT DEFAULT 0 not null, primary key (id)) engine=MyISAM ;
create table decision_archive (id INT not null auto_increment, checksum varchar(255) not null, created datetime not null, name varchar(255) not null, provider varchar(255) not null, unpacked bit not null, primary key (id)) engine=MyISAM ;
create table decision_archive_file (id bigint not null auto_increment, archive_id INT NOT NULL, colision bit not null, decision_id bigint, duplicated bit not null, path varchar(255) not null, processed datetime, file_id bigint, primary key (id)) engine=MyISAM ;
create table decision_file (id bigint not null auto_increment, active bit not null, checksum varchar(255) not null, converted bit not null, corrupted bit not null, created datetime, decision_id INT NOT NULL not null, filename varchar(255), modified datetime, output bit not null, source varchar(20) not null, type varchar(10) not null, version integer not null, origin_id bigint, primary key (id)) engine=MyISAM ;
create table decision_property (datatype TINYINT not null, id bigint not null auto_increment, created datetime not null, date_value datetime, deleted bit not null, fetching SMALLINT DEFAULT 0 not null, previously bigint, string_value varchar(255), text_value MEDIUMTEXT, version SMALLINT DEFAULT 0 not null, decision_id INT not null, metadata_id SMALLINT not null, preset_id MEDIUMINT, primary key (id)) engine=MyISAM ;
create table eurlex_meta (id INT not null auto_increment, date datetime not null, ecli varchar(255) not null, format varchar(255) not null, gps_id varchar(255) not null, id_larcier varchar(255) not null, langue varchar(255) not null, primary key (id)) engine=MyISAM ;
create table preset_value (datatype TINYINT not null, id MEDIUMINT not null auto_increment, candidate bit not null, created datetime not null, date_value datetime, string_value varchar(255), metadata_id SMALLINT not null, primary key (id)) engine=MyISAM ;
create table property_metadata (id SMALLINT not null auto_increment, created datetime not null, editable bit not null, incremental bit, multiple bit not null, name varchar(255) not null, obsoleted datetime, optional bit not null, parser varchar(255), preset bit not null, priority SMALLINT DEFAULT 0 not null, up datetime not null, xpath varchar(255) not null, primary key (id)) engine=MyISAM ;
create index IDX39158g0dhs721576m1wppxc6d on decision (modified) ;
create index IDX4xp9p6wcdcf89qx8ec52qxoyn on decision (created) ;
create index IDX2c4hpsys2iubo4ic5cyb2y31p on decision (version) ;
alter table decision_archive add constraint UK_c4ym5u2e1e8wam8ii0tsrvumy unique (checksum) ;
alter table decision_archive_file add constraint UK1vebtsxhn7xw2wf4sh41oh7js unique (archive_id, path) ;
create index IDXlvr43erfrxxuxln5nxjvsbm8s on decision_file (decision_id, type) ;
create index IDXtc2vs8dyoxif393c46n0iji1c on decision_file (source, type, checksum) ;
create index IDXmnx5dep6n7cxs3flwad7qs7uq on decision_file (filename, source) ;
create index IDX68x1sa0dvfn3oi933hdwto1jw on decision_file (created) ;
create index IDXgr0i6qgidw8lolgqrlppxrti0 on decision_file (modified) ;
create index IDXa1q7tevh3iebghxorqtvu8h1i on decision_file (type) ;
create index IDXiooosuoyhi4vp2mxegbmm7c6f on decision_file (checksum) ;
create index IDXea8hfv2ujprcrssvhi32shjdo on decision_file (source, type, converted) ;
create index IDXp1jt4w1n8oxnjr0k7pbeauuwe on decision_property (string_value) ;
create index IDXmysd21gk7ouxwu8366b7w3jle on decision_property (date_value) ;
alter table property_metadata add constraint UK_9x02pux4hecox4wxdfblwd9te unique (name) ;
alter table decision_archive_file add constraint FKea5p3pr36l0fnhr0oc31ud0rv foreign key (archive_id) references decision_archive (id) ;
alter table decision_archive_file add constraint FK4yxb7ttm32d46ayi8c690ptk foreign key (file_id) references decision_file (id) ;
alter table decision_file add constraint FKe9ff3j50ml9ubvl0od663slro foreign key (decision_id) references decision (id) ;
alter table decision_file add constraint FKpwvgex76otfbm989gl4ehboy7 foreign key (origin_id) references decision_file (id) ;
alter table decision_property add constraint FKkfy3j1n8ii0oxa7nybov0m648 foreign key (decision_id) references decision (id) ;
alter table decision_property add constraint FKo7nfpxe04594hk91pwnv6alws foreign key (metadata_id) references property_metadata (id) ;
alter table decision_property add constraint FK8q68ln6gceu9ht6uqhxd5xqsn foreign key (preset_id) references preset_value (id) ;
alter table preset_value add constraint FKaxd108yqmbxv6qo4ir3cditck foreign key (metadata_id) references property_metadata (id) ;
insert into gps_datastore.property_metadata values (1,"2013-10-01 00:00:00",0,0,0,"cle_",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/@cle");
insert into gps_datastore.property_metadata values (2,"2013-10-01 00:00:00",1,0,0,"J_DT_Juridiction",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/Juridiction");
insert into gps_datastore.property_metadata values (3,"2013-10-01 00:00:00",1,0,0,"J_DT_Lieu",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/Lieu");
insert into gps_datastore.property_metadata values (4,"2013-10-01 00:00:00",1,0,0,"J_DT_Formation",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/Formation");
insert into gps_datastore.property_metadata values (5,"2013-10-01 00:00:00",1,0,0,"J_DT_Date",NULL,0,"ISO_DATE",0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/Date");
insert into gps_datastore.property_metadata values (6,"2013-10-01 00:00:00",0,0,0,"J_DT_DateNonNormalisee",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/DateNonNormalisee");
insert into gps_datastore.property_metadata values (7,"2013-10-01 00:00:00",1,0,0,"J_DT_Numero",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/NumerosJuri/Numero[1]");
insert into gps_datastore.property_metadata values (8,"2013-10-01 00:00:00",1,0,1,"J_DT_NumeroDiffusion",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/NumerosDiffusion/Numero");
insert into gps_datastore.property_metadata values (9,"2013-10-01 00:00:00",1,0,0,"J_DT_Nature",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/Nature");
insert into gps_datastore.property_metadata values (10,"2013-10-01 00:00:00",1,0,0,"J_DT_FormationDiffusion",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/FormationDiffusion");
insert into gps_datastore.property_metadata values (11,"2013-10-01 00:00:00",1,0,0,"J_DT_RenvoiHauteAutorite",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/RenvoiHauteAutorite");
insert into gps_datastore.property_metadata values (12,"2013-10-01 00:00:00",1,0,0,"J_DT_Recours",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/Recours");
insert into gps_datastore.property_metadata values (13,"2013-10-01 00:00:00",0,0,0,"J_DT_TypeJuri",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/TypeJuri");
insert into gps_datastore.property_metadata values (14,"2013-10-01 00:00:00",0,0,0,"J_DT_ECLI",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/ECLI");
insert into gps_datastore.property_metadata values (15,"2013-10-01 00:00:00",0,0,0,"J_DT_CELEX",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/CELEX");
insert into gps_datastore.property_metadata values (16,"2013-10-01 00:00:00",0,0,0,"J_DT_NOR",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/NOR");
insert into gps_datastore.property_metadata values (17,"2013-10-01 00:00:00",1,0,0,"J_DD_Juridiction",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/Juridiction");
insert into gps_datastore.property_metadata values (18,"2013-10-01 00:00:00",1,0,0,"J_DD_Lieu",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/Lieu");
insert into gps_datastore.property_metadata values (19,"2013-10-01 00:00:00",1,0,0,"J_DD_Date",NULL,0,"ISO_DATE",0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/Date");
insert into gps_datastore.property_metadata values (20,"2013-10-01 00:00:00",0,0,0,"J_DD_DateNonNormalisee",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/DateNonNormalisee");
insert into gps_datastore.property_metadata values (21,"2013-10-01 00:00:00",1,0,0,"J_DD_Numero",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/NumerosJuri/Numero[1]");
insert into gps_datastore.property_metadata values (22,"2013-10-01 00:00:00",1,0,0,"J_DD_Nature",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/Nature");
insert into gps_datastore.property_metadata values (23,"2013-10-01 00:00:00",1,0,0,"J_DD_RenvoiHauteAutorite",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/RenvoiHauteAutorite");
insert into gps_datastore.property_metadata values (24,"2013-10-01 00:00:00",0,0,1,"J_Demandeurs",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/Demandeurs/NomPartie/Texte");
insert into gps_datastore.property_metadata values (25,"2013-10-01 00:00:00",0,0,1,"J_Defendeurs",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/Defendeurs/NomPartie/Texte");
insert into gps_datastore.property_metadata values (26,"2013-10-01 00:00:00",1,0,1,"J_Avocats",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/Avocats/Avocat");
insert into gps_datastore.property_metadata values (27,"2013-10-01 00:00:00",1,0,1,"J_Avoues",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/Avoues/Avoue");
insert into gps_datastore.property_metadata values (28,"2013-10-01 00:00:00",0,0,0,"J_CC_President",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/CompositionCour/President");
insert into gps_datastore.property_metadata values (29,"2013-10-01 00:00:00",1,0,1,"J_CC_Conseiller",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/CompositionCour/Conseiller");
insert into gps_datastore.property_metadata values (30,"2013-10-01 00:00:00",1,0,0,"J_CC_AvocatGeneral",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/CompositionCour/AvocatGeneral");
insert into gps_datastore.property_metadata values (31,"2013-10-01 00:00:00",1,0,0,"J_CC_Greffier",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/CompositionCour/Greffier");
insert into gps_datastore.property_metadata values (32,"2013-10-01 00:00:00",0,0,0,"J_CC_CommissaireGouvernement",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/CompositionCour/CommissaireGouvernement");
insert into gps_datastore.property_metadata values (33,"2013-10-01 00:00:00",0,0,0,"J_CC_Rapporteur",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/CompositionCour/Rapporteur");
insert into gps_datastore.property_metadata values (34,"2013-10-01 00:00:00",0,0,0,"J_FT_NOR",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/GroupeFondTextes/FondTextes/NOR");
insert into gps_datastore.property_metadata values (35,"2013-10-01 00:00:00",0,0,0,"J_FT_Titre",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/GroupeFondTextes/FondTextes/Titre");
insert into gps_datastore.property_metadata values (36,"2013-10-01 00:00:00",0,0,0,"J_FT_Nature",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/GroupeFondTextes/FondTextes/Nature");
insert into gps_datastore.property_metadata values (37,"2013-10-01 00:00:00",0,0,0,"J_FT_Date",NULL,0,"ISO_DATE",0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/GroupeFondTextes/FondTextes/Date");
insert into gps_datastore.property_metadata values (38,"2013-10-01 00:00:00",0,0,0,"J_FT_DateNonNormalisee",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/GroupeFondTextes/FondTextes/DateNonNormalisee");
insert into gps_datastore.property_metadata values (39,"2013-10-01 00:00:00",0,0,0,"J_FT_Numero",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/GroupeFondTextes/FondTextes/Numero");
insert into gps_datastore.property_metadata values (40,"2013-10-01 00:00:00",0,0,1,"J_FT_Articles",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/GroupeFondTextes/FondTextes/Articles/Article");
insert into gps_datastore.property_metadata values (41,"2013-10-01 00:00:00",0,0,0,"J_PO_Reference",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/PublicationOfficielle/Reference");
insert into gps_datastore.property_metadata values (42,"2013-10-01 00:00:00",0,0,0,"J_PO_CodePublication",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/PublicationOfficielle/CodePublication");
insert into gps_datastore.property_metadata values (43,"2013-10-01 00:00:00",0,0,0,"J_Solution",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/Solution");
insert into gps_datastore.property_metadata values (44,"2013-10-01 00:00:00",0,0,1,"D_Saisines",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaDoc/Saisines");
insert into gps_datastore.property_metadata values (45,"2013-10-01 00:00:00",0,0,1,"D_D_MotsClesPrincipaux",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaDoc/Descriptions/Description/MotsClesPrincipaux/MotCle");
insert into gps_datastore.property_metadata values (46,"2013-10-01 00:00:00",0,0,1,"D_D_origine_",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaDoc/Descriptions/Description/@origine");
insert into gps_datastore.property_metadata values (47,"2013-10-01 00:00:00",0,0,1,"D_D_Resume",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaDoc/Descriptions/Description/Resume");
insert into gps_datastore.property_metadata values (48,"2013-10-01 00:00:00",0,0,1,"D_D_MotsClesSecondaires",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaDoc/Descriptions/Description/MotsClesSecondaires/MotCle");
insert into gps_datastore.property_metadata values (49,"2013-10-01 00:00:00",0,0,1,"D_CitationJuri",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaDoc/CitationJuri/P");
insert into gps_datastore.property_metadata values (50,"2013-10-01 00:00:00",0,0,1,"D_CitationTexte",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaDoc/CitationTexte/P");
insert into gps_datastore.property_metadata values (51,"2013-10-01 00:00:00",0,0,0,"F_S_ID",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Source/Id");
insert into gps_datastore.property_metadata values (52,"2013-10-01 00:00:00",0,0,0,"F_S_Origine",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Source/Origine");
insert into gps_datastore.property_metadata values (53,"2013-10-01 00:00:00",0,0,0,"F_S_DateExtraction",NULL,0,"ISO_DATETIME",0,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Source/DateExtraction");
insert into gps_datastore.property_metadata values (54,"2013-10-01 00:00:00",0,0,0,"F_D_Cle",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Document/Cle");
insert into gps_datastore.property_metadata values (55,"2013-10-01 00:00:00",0,0,0,"F_D_D_Creation",NULL,0,"ISO_DATETIME",0,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Document/Dates/Creation");
insert into gps_datastore.property_metadata values (56,"2013-10-01 00:00:00",0,0,0,"F_D_D_Modification",NULL,0,"ISO_DATETIME",0,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Document/Dates/Modification");
insert into gps_datastore.property_metadata values (57,"2013-10-01 00:00:00",0,0,0,"F_D_Etat",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Document/Etat");
insert into gps_datastore.property_metadata values (58,"2013-10-01 00:00:00",0,0,0,"F_D_Qualification",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Document/Qualification");
insert into gps_datastore.property_metadata values (59,"2013-10-01 00:00:00",0,0,0,"J_DT_ArretProcedure",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionTraitee/ArretProcedure");
insert into gps_datastore.property_metadata values (60,"2013-10-01 00:00:00",0,0,1,"J_DD_NumeroDiffusion",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/NumerosDiffusion/Numero");
insert into gps_datastore.property_metadata values (61,"2013-10-01 00:00:00",0,0,0,"J_DD_FormationDiffusion",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/FormationDiffusion");
insert into gps_datastore.property_metadata values (62,"2013-10-01 00:00:00",0,0,0,"J_DD_Recours",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/Recours");
insert into gps_datastore.property_metadata values (63,"2013-10-01 00:00:00",0,0,0,"J_DD_TypeJuri",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/TypeJuri");
insert into gps_datastore.property_metadata values (64,"2013-10-01 00:00:00",0,0,0,"J_DD_ECLI",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/ECLI");
insert into gps_datastore.property_metadata values (65,"2013-10-01 00:00:00",0,0,0,"J_DD_CELEX",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/CELEX");
insert into gps_datastore.property_metadata values (66,"2013-10-01 00:00:00",0,0,0,"J_DD_NOR",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/NOR");
insert into gps_datastore.property_metadata values (67,"2013-10-01 00:00:00",0,0,0,"J_DD_ArretProcedure",NULL,0,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/ArretProcedure");
insert into gps_datastore.property_metadata values (68,"2013-10-01 00:00:00",1,0,0,"J_DD_Formation",NULL,0,NULL,1,0,"2013-10-01 00:00:00","/Juri/MetaJuri/DecisionDeferee/Formation");
insert into gps_datastore.property_metadata values (69,"2013-10-01 00:00:00",0,0,1,"F_C_C_Commentaire",NULL,1,NULL,0,0,"2013-10-01 00:00:00","/Juri/MetaFlux/Corrections/Commentaire");

