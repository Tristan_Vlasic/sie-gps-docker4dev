docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root mysql_gps
docker run -d -p 32782:8983 -P -v $PWD/resources/solr/core/:/config solr:5.5.5-slim solr-create -c DecisionIndex -d /config
docker run -d -p 8080:8080 --add-host outside:172.17.0.1 util_datastore
docker run -d -p 8082:8080 --add-host outside:172.17.0.1 indexing
